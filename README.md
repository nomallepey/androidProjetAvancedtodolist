# androidProjetAvancedToDoList

Nourrane MALLEPEYRE

# ToDoList 
## Gestionnaire de Tâches
![img_5.png](img_5.png)
![img_8.png](img_8.png)
![img_1.png](img_1.png)
![img_7.png](img_7.png)
![img_6.png](img_6.png)
![img_5.png](img_5.png)


# Description

Le Gestionnaire de Tâches est une application Android développée avec Android Studio. Elle permet aux utilisateurs de gérer leurs tâches quotidiennes avec facilité. Les principales fonctionnalités incluent l'ajout, la modification, la suppression de tâches, le drag-and-drop, l'ajout de dates optionnelles, l'indication des tâches en retard, le filtrage des tâches terminées et l'indication de l'état de chaque tâche (faite ou non).
Fonctionnalités

1. Ajout de Tâche: Les utilisateurs peuvent ajouter de nouvelles tâches avec une description et une date optionnelle.

2. Modification de Tâche: Les utilisateurs peuvent modifier les détails d'une tâche existante.

3. Suppression de Tâche: Les utilisateurs peuvent supprimer des tâches de leur liste.

4. Drag-and-Drop: Les utilisateurs peuvent réorganiser l'ordre des tâches par un simple glisser-déposer.

5. Ajout d'une Date Optionnelle: Lors de l'ajout ou de la modification d'une tâche, les utilisateurs peuvent spécifier une date d'échéance.

6. Indication des Tâches en Retard: Les tâches dont la date d'échéance est passée sont marquées comme étant en retard.

7. Filtre sur les Tâches Terminées: Les utilisateurs peuvent filtrer la liste pour afficher uniquement les tâches terminées.

8. Indication de l'État de la Tâche: Chaque tâche a une indication visuelle pour montrer si elle est faite ou non.

Installation

 - Clonez ce dépôt sur votre machine locale:

 ``` 
  https://gitlab.isima.fr/nomallepey/androidProjetAvancedtodolist.git
  ```
 - Ouvrez le projet avec Android Studio.

 - Compilez et exécutez l'application sur un émulateur ou un appareil physique.

### Utilisation
#### Ajouter une Tâche

    Appuyez sur le bouton "+" en bas à doite.
    Entrez la description de la tâche.
    Optionnellement, choisissez une date d'échéance.
    Appuyez sur "Sauvegarder".

#### Modifier une Tâche

    Cliquer sur le bouton le plus à droite de la tâche à modifier.
    Cliquer sur "Modifier"
    Changez les détails selon vos besoins.
    Appuyez sur "Sauvegarder".

#### Supprimer une Tâche
    Cliquer sur le bouton le plus à droite de la tâche à modifier.
    Cliquer sur "Supprimer"

#### Drag-and-Drop

    Appuyez longuement sur une tâche.
    Faites-la glisser à la position désirée.

#### Filtrer les Tâches Terminées
    Swipper vers la droite ou cliquer sur le bouton "Réglage"
    Valider la modification

#### Supprimer un filtre
    Ciquer sur le filtre en haut de l'écran

#### Supprimer tout les filtres
    Cliquer sur le bouton "Réglage"
    Cliquer sur "Supprimer tout les filtres"

#### Indication de l'État de la Tâche
    Les tâches terminées sont marquées avec un label "Fait".
    Un label "En retard" apparait lorsque la date de la tâche est dépassée.

## Fonctionnalités Futures

- Notification si Date Dépassée pour une Tâche: Recevez des notifications lorsqu'une tâche est en retard.

- Dossiers avec Plusieurs Todo Lists: Créez et gérez plusieurs listes de tâches sous différents dossiers pour une meilleure organisation.

- Pourcentage de Tâches Restant à Faire avec Barre de Complétion: Visualisez le pourcentage de tâches restantes avec une barre de progression.

- Bouton de Suppression de Toutes les Tâches Terminées: Supprimez facilement toutes les tâches terminées en un seul clic.

- Filtres sur l'Importance des Tâches: Filtrez les tâches en fonction de leur importance (haute, moyenne, basse).

- Swipe pour Supprimer une Tâche: Supprimez une tâche en la faisant glisser vers la gauche.