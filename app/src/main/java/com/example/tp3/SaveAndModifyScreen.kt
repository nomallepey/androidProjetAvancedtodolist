package com.example.tp3

import android.app.DatePickerDialog
import android.os.Build
import android.widget.DatePicker
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.MenuAnchorType
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.util.Calendar
import java.util.Date

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun Screen2(navController: NavController, taskManager: TaskListManager, taskId: Any?){
    val task = taskManager.tasks.value?.firstOrNull { it.id == taskId }
    var text by remember { mutableStateOf(if (task?.text != null) task.text else "") }
    var logo by remember { mutableIntStateOf(if (task?.logo != null) task.logo else R.drawable.icons_low_priority) }
    var deadline by remember { mutableStateOf(if (task?.deadLine != null) task.deadLine else null) }

    Column(
        modifier = Modifier
            .padding(16.dp, 16.dp)
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Text(
            color = Color(0xFF341172),
            text = if (taskId == "newTask") "Création d'une tâche" else "Modification de la tâche",
            fontWeight = FontWeight.Bold,
            style = MaterialTheme.typography.displaySmall
        )
        Spacer(modifier = Modifier.padding(8.dp))
        Text(
            color = Color(0xFF341172),
            text = "Description de la tâche",
            style = MaterialTheme.typography.bodyLarge
        )
        TextField(
            value = text,
            onValueChange = { text = it },
            modifier = Modifier.fillMaxWidth(),
            placeholder = { Text(text = "ex: Faire les courses", color = Color.DarkGray, fontStyle = FontStyle.Italic) }
        )
        Spacer(modifier = Modifier.padding(4.dp))
        PriorityDropdownMenu(taskManager, logo) { logo = it }
        Spacer(modifier = Modifier.padding(4.dp))
        CheckboxTaskWithDeadline(task?.deadLine) { deadline = it }
        Spacer(modifier = Modifier.padding(4.dp))
        Button(
            enabled = text.isNotEmpty(),
            colors = ButtonColors(Color(0xFF8e5ce6), Color.White, Color.DarkGray, Color.White),
            onClick = {
                taskManager.update(
                    task?.id,
                    text,
                    logo,
                    deadline
                ); navController.navigate("DisplayTasks")
            },
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp)
                .align(Alignment.CenterHorizontally),
        ) {
            Text(text = "Sauvegarder la tâche")
        }
        OutlinedButton(
            colors = ButtonDefaults.outlinedButtonColors(
                Color.White,
                Color(0xFF8e5ce6),
                Color.DarkGray,
                Color.White
            ),
            onClick = { navController.navigate("DisplayTasks") },
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp)
                .align(Alignment.CenterHorizontally)
        ) {
            Text(text = "Annuler ")
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CheckboxTaskWithDeadline(oldDeadline: Long?, deadline: (Long) -> Unit){
    var checked by remember { mutableStateOf(oldDeadline != null) }
    var endDate by remember { mutableLongStateOf(0L) }
    if (oldDeadline !=null) deadline(oldDeadline)
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            color = Color(0xFF341172),
            text = "Ajout d'une date limite pour la tâche"
        )
        Checkbox(
            checked = checked,
            onCheckedChange = { checked = it }
        )
    }

    if (checked) {
        EndDateTextField(oldDeadline) { date ->
            endDate = date
            deadline(endDate)
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PriorityDropdownMenu(taskManager: TaskListManager, oldLogo : Int , setlogo: (Int) -> Unit) {
    val labels = arrayOf( "Importance faible", "Importance moyenne", "Importance forte")
    Column(
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Text(
            color = Color(0xFF341172),
            text = "Priorité de la tâche ",
            style = MaterialTheme.typography.bodyLarge
        )

        var expanded by remember { mutableStateOf(false) }
        var logo by remember { mutableIntStateOf(oldLogo) }
        ExposedDropdownMenuBox(
            modifier = Modifier.fillMaxWidth(),
            expanded = expanded,
            onExpandedChange = { expanded = !expanded },
        ) {
            TextField(
                value = labels[taskManager.logos.indexOf(logo)],
                leadingIcon = {
                    Image(
                        painter = painterResource(id = logo),
                        contentDescription = null
                    )
                },
                modifier = Modifier
                    .menuAnchor(MenuAnchorType.PrimaryNotEditable, true)
                    .fillMaxWidth(),
                readOnly = true,
                onValueChange = {},
                trailingIcon = {
                    ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded)
                },
                colors = ExposedDropdownMenuDefaults.textFieldColors(),
            )
            ExposedDropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
            ) {
                taskManager.logos.forEach { selectionOption ->
                    DropdownMenuItem(
                        leadingIcon = {
                            Image(
                                painter = painterResource(id = selectionOption),
                                contentDescription = null
                            )
                        },
                        text = { Text(labels[taskManager.logos.indexOf(selectionOption)]) },
                        onClick = {
                            logo = selectionOption
                            setlogo(logo)
                            expanded = false
                        }
                    )
                }
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
fun getYearFromMillis(millis: Long): LocalDate? {

    return Instant.ofEpochMilli(millis).atZone(ZoneId.systemDefault()).toLocalDate()
}
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun EndDateTextField(oldDate: Long? ,endDate: (Long) -> Unit) {
    Text(
        text = "Date limite",
        style = MaterialTheme.typography.bodyLarge
    )

    val interactionSource = remember { MutableInteractionSource() }
    val isPressed: Boolean by interactionSource.collectIsPressedAsState()

    val currentDate = Date().toFormattedString()
    var selectedDate by rememberSaveable { mutableStateOf(if (oldDate!=null) Date(oldDate).toFormattedString() else currentDate) }

    val context = LocalContext.current

    val calendar = Calendar.getInstance()
    val year: Int = oldDate?.let { getYearFromMillis(it)?.year } ?:calendar.get(Calendar.YEAR)
    val month: Int = oldDate?.let { getYearFromMillis(it)?.monthValue } ?:calendar.get(Calendar.MONTH)
    val day: Int = oldDate?.let { getYearFromMillis(it)?.dayOfMonth } ?:calendar.get(Calendar.DAY_OF_MONTH)
    calendar.time = Date()
    val datePickerDialog =
        DatePickerDialog(
            context,
            { _: DatePicker, year: Int, month: Int, dayOfMonth: Int ->
                val newDate = Calendar.getInstance()
                newDate.set(year, month, dayOfMonth)
                selectedDate = " $dayOfMonth ${month.toMonthName()} $year"
                endDate((newDate.time).time)
            },
            year,
            month,
            day
        )

    TextField(
        modifier = Modifier.fillMaxWidth(),
        readOnly = true,
        value = selectedDate,
        onValueChange = {},
        trailingIcon = { Icons.Default.DateRange },
        interactionSource = interactionSource
    )

    if (isPressed) {
        datePickerDialog.show()
    }
}