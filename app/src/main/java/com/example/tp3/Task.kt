package com.example.tp3
import kotlinx.serialization.Serializable




@Serializable
data class Task(val id: String, val text: String, val logo: Int,  val deadLine: Long?, val toDo: Boolean)
