package com.example.tp3

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.MenuAnchorType
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun Filter(taskManager: TaskListManager, closeDrawer: () -> Unit){
    var toDoFilter by remember {mutableStateOf("...") }

    ModalDrawerSheet {
        Column {
            Text(
                color = Color(0xFF341172),
                text = "Filtrer la liste des tâches",
                fontWeight = FontWeight.Bold,
                style = MaterialTheme.typography.displaySmall
            )
            Spacer(modifier = Modifier.padding(8.dp))
            TodoDropdownMenu(toDoFilter) { toDoFilter = it }
            Spacer(modifier = Modifier.padding(8.dp))
            Button(
                colors = ButtonColors(Color(0xFF8e5ce6), Color.White, Color.DarkGray, Color.White),
                onClick = { taskManager.applyAllFilter(todo = toDoFilter)},
                modifier = Modifier
                    .fillMaxWidth()
                    .height(56.dp)
                    .align(Alignment.CenterHorizontally),
            ) {
                Text(text = "Appliquer les filtres")
            }
            Spacer(modifier = Modifier.padding(8.dp))
            Button(
                colors = ButtonDefaults.outlinedButtonColors(
                    Color.White,
                    Color(0xFF8e5ce6),
                    Color.DarkGray,
                    Color.White
                ),
                onClick = { closeDrawer()},
                modifier = Modifier
                    .fillMaxWidth()
                    .height(56.dp)
                    .align(Alignment.CenterHorizontally),
            ) {
                Text(text = "Annuler")
            }
            Spacer(modifier = Modifier.padding(8.dp))
            OutlinedButton(
                colors = ButtonDefaults.outlinedButtonColors(
                    Color.White,
                    Color(0xFF8e5ce6),
                    Color.DarkGray,
                    Color.White
                ),
                onClick = { taskManager.deleteAllFilters()},
                modifier = Modifier
                    .fillMaxWidth()
                    .height(56.dp)
                    .align(Alignment.CenterHorizontally),
            ) {
                Text(text = "Supprimer tout les filtres")
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TodoDropdownMenu( currentTodoStatus: String, setFilter: (String) -> Unit) {
    val labels = arrayOf(Pair("...", "..."), Pair("Tâches à faire", "ToDo"), Pair("Tâches faites", "Done"))
    Column(
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Text(
            color = Color(0xFF341172),
            text = "Status des tâches",
            style = MaterialTheme.typography.bodyLarge
        )

        var expanded by remember { mutableStateOf(false) }
        var toDoFilter by remember { mutableStateOf(currentTodoStatus) }
        ExposedDropdownMenuBox(
            modifier = Modifier.fillMaxWidth(),
            expanded = expanded,
            onExpandedChange = { expanded = !expanded },
        ) {
            TextField(
                value = toDoFilter,
                modifier = Modifier
                    .menuAnchor(MenuAnchorType.PrimaryNotEditable, true)
                    .fillMaxWidth(),
                readOnly = true,
                onValueChange = {},
                trailingIcon = {
                    ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded)
                },
                colors = ExposedDropdownMenuDefaults.textFieldColors(),
            )
            ExposedDropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
            ) {
                labels.forEach { selectionOption ->
                    DropdownMenuItem(
                        text = { Text(selectionOption.first) },
                        onClick = {
                            toDoFilter = selectionOption.first
                            setFilter(selectionOption.second)
                            expanded = false
                        }
                    )
                }
            }
        }
    }
}