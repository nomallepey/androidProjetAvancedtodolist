package com.example.tp3

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.serialization.json.Json
import kotlinx.serialization.encodeToString
import java.io.File
import java.util.UUID
import kotlin.random.Random


class TaskListManager: ViewModel() {
    private val tasksList: MutableLiveData<List<Task>>  = MutableLiveData()
    private val filterList: MutableLiveData<List<String>>  = MutableLiveData()
    private val displayedTasksList: MutableLiveData<List<Task>> = MutableLiveData()
    private val listLogo: IntArray = intArrayOf( R.drawable.icons_low_priority, R.drawable.icons_medium_priority, R.drawable.icons_high_priority)
    private val filename : String = "StockageFile"
    @SuppressLint("StaticFieldLeak")
    private var context : Context? = null

    fun initialise(newContext : Context) {
        context = newContext
        if (context != null && !fileExists(context!!, filename)) {
            initTasksList()
        }
        getTasksSaved()
    }

    private fun fileExists(context: Context, filename: String): Boolean {
        val file = File(context.filesDir, filename)
        return file.exists()
    }

    fun initTasksList(){
        val labels = arrayOf("Acheter de la crème solaire", "Laver les serviettes de plage", "Faire la réservation du AirBnb", "Prevenir ses proches", "Prendre les billets d'avion", "Changer d'avis", "Aller élever des chèvres dans le Jura" )
        for( i in 0..6){
            addTasks(labels[i], listLogo[Random.nextInt(0, listLogo.count())], System.currentTimeMillis())
        }
        addTasks("Les idios critiques les personnes, les intelligents interprètent les actions, les sages discute des idées", listLogo[ Random.nextInt(0, listLogo.count())], null)
    }

    val filtersList: LiveData<List<String>>
        get() = filterList
    val tasks: LiveData<List<Task>>
        get() = displayedTasksList

    val logos: IntArray
        get() = listLogo

    fun updateTasks(newTasks: List<Task>) {
        tasksList.value = newTasks
        saveTasks()
    }

    fun deleteAllFilters(){
        filterList.value = emptyList()
        displayedTasksList.value = tasksList.value.orEmpty().toMutableList()
    }

    private fun applyFiltersSaved(){
        if (filterList.value?.isNotEmpty() != true) {
            displayedTasksList.value = tasksList.value
        }
        filterList.value?.forEach { filter -> applyAllFilter(filter)}
    }
    fun applyAllFilter(todo: String){
        displayedTasksList.value = applyToDoFilter(todo)?.toList()
    }

    fun deleteOneFilter(filter: String){
        val list = filterList.value.orEmpty().toMutableList()
        list.remove(filter)
        filterList.value = list
        applyFiltersSaved()
    }

    private fun addFilterToDo(){
        val list = filterList.value.orEmpty().toMutableList()
        if (list.none { it == "A FAIRE" })
            list.add("A FAIRE")
        list.remove("FAIT")
        filterList.value = list;
    }

    private fun addFilterDone(){
        val list = filterList.value.orEmpty().toMutableList()
        if (list.none { it == "FAIT" })
            list.add("FAIT")
        list.remove("A FAIRE")
        filterList.value = list;
    }

    private fun applyToDoFilter(text: String): List<Task>? {
        val list = tasksList.value.orEmpty().toMutableList()
        if (text == "ToDo") {
            addFilterToDo()

            return list.filter { it.toDo };
        }
        else if (text == "Done") {
            addFilterDone()
            return list.filter { !it.toDo };
        }
        else deleteAllFilters()

        return tasksList.value;
    }

    private fun addTasks(text: String, priority: Int, deadline: Long?) {
        val list = tasksList.value.orEmpty().toMutableList()
        list.add(Task(UUID.randomUUID().toString(), text, priority, deadline, true))
        tasksList.value = list;
        context?.let { saveTasks() }
    }

    fun remove(task : Task){
        val list = tasksList.value.orEmpty().toMutableList()
        if (!list.contains(task)) return
        list.remove(task)
        tasksList.value = list
        context?.let { saveTasks() }

    }

    private fun saveTasks(){
        val fileContents = Json.encodeToString(tasksList.value)
        context?.openFileOutput(filename, Context.MODE_PRIVATE).use {
            it?.write(fileContents.toByteArray())
        }
        applyFiltersSaved()
    }

    fun getTasksSaved(){
        context?.openFileInput(filename).use { stream ->
            if (stream != null) {
                tasksList.value = stream.bufferedReader().use {
                    Json.decodeFromString<List<Task>>(it.readText())
                }
            }
        }
        displayedTasksList.value = tasksList.value
    }

    fun updateIsDoneStatusOfTask(task: Task){
        val i = tasksList.value?.indexOf(task) ?: -1
        val updatedTask = task.copy(toDo = !task.toDo)
        val newList = tasksList.value?.toMutableList() ?: mutableListOf()
        newList[i] = updatedTask
        tasksList.value = newList
        saveTasks()
    }

    fun update(id: String?, text: String, logo: Int, deadline: Long?) {
        val task = tasksList.value?.firstOrNull { it.id == id }
        if (id == null || task == null) {
            addTasks(text, logo, deadline)
        } else {
            val i = tasksList.value?.indexOf(task) ?: -1
            if (i != -1) {
                val updatedTask = task.copy(text = text, logo = logo, deadLine = deadline)
                val newList = tasksList.value?.toMutableList() ?: mutableListOf()
                newList[i] = updatedTask
                tasksList.value = newList
            }
        }
        saveTasks()
    }
}