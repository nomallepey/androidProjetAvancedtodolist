package com.example.tp3

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.rounded.Delete
import androidx.compose.material.icons.rounded.Edit
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FabPosition
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LargeFloatingActionButton
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import org.burnoutcrew.reorderable.*
import com.example.tp3.ui.theme.Tp3Theme
import kotlinx.coroutines.launch
import org.burnoutcrew.reorderable.ReorderableItem
import org.burnoutcrew.reorderable.detectReorderAfterLongPress
import org.burnoutcrew.reorderable.rememberReorderableLazyListState
import org.burnoutcrew.reorderable.reorderable
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.util.Date
import java.util.Locale


class MainActivity : ComponentActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Tp3Theme {
                Surface(
                    modifier = Modifier.fillMaxSize()
                ) {
                    MainFunction()
                }
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun MainFunction(){
    val navController = rememberNavController()
    val tasksManager = TaskListManager()
    val c = LocalContext.current
    tasksManager.initialise(c)
    NavHost(navController = navController, startDestination = "DisplayTasks") {
        composable("DisplayTasks") { Screen1( navController = navController, tasksManager)}
        composable("AddTask/{taskId}") { backStackEntry ->
            Screen2( navController = navController, tasksManager, backStackEntry.arguments?.getString("taskId"))}
    }
}
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun Screen1(navController : NavController,taskManager : TaskListManager ){
    TopAppBarTasks(taskManager, navController)
}

fun Int.toMonthName(): String {
    return DateFormatSymbols().months[this]
}

fun Date.toFormattedString(): String {
    val simpleDateFormat = SimpleDateFormat("dd LLLL yyyy", Locale.getDefault())
    return simpleDateFormat.format(this)
}

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopAppBarTasks(taskManager : TaskListManager, navController : NavController) {
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val filtersListState by taskManager.filtersList.observeAsState(initial = emptyList() )
    val scope = rememberCoroutineScope()
    ModalNavigationDrawer(
        drawerState = drawerState,
        drawerContent = {
            Filter(taskManager) {
                scope.launch {
                    drawerState.close()
                }
            }
        },
    ) {

        Scaffold(
            topBar = {
                TopAppBar(
                    expandedHeight = if (filtersListState.any()) 90.dp else 60.dp,
                    colors = TopAppBarDefaults.topAppBarColors(
                        containerColor = Color(0xFF341172),
                        titleContentColor = Color(0xFFbca4f4),
                    ),
                    title = {
                        Column{
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                                horizontalArrangement = Arrangement.SpaceBetween,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                Text("Tâches à réaliser")
                                Row{
                                    IconButton(onClick = {  navController.navigate("AddTask/newTask") }) {
                                    Icon(Icons.Default.AddCircle, "Add", modifier = Modifier.size(30.dp))
                                }
                                    IconButton(
                                        onClick = {
                                            scope.launch {
                                                if (drawerState.isClosed) {
                                                    drawerState.open()
                                                } else {
                                                    drawerState.close()
                                                }
                                            }
                                        },
                                        modifier = Modifier.padding(10.dp, 0.dp)
                                    ) {
                                        Image(
                                            painterResource(id = R.drawable.tune_24dp_fill0_wght400_grad0_opsz24),
                                            contentDescription = "filter",
                                        )
                                    }}

                            }
                            if (filtersListState.any())
                                Row(
                                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                                )
                                {
                                    filtersListState
                                        .forEach { filter ->
                                            FilterChip(
                                                colors = FilterChipDefaults.filterChipColors(Color.White),
                                                onClick = {taskManager.deleteOneFilter(filter)},
                                                label = { Text(text = filter) },
                                                selected = false,
                                                leadingIcon = {
                                                    Icon(
                                                        imageVector = Icons.Default.Close,
                                                        contentDescription = "Selected"
                                                    )
                                                }
                                            )
                                        }
                                }
                        }
                    }
                )
            },
        ) { innerPadding ->
            VerticalReorderList(taskManager, navController, innerPadding)
        }
    }
}
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun VerticalReorderList(
    taskManager: TaskListManager,
    navController: NavController,
    innerPadding: PaddingValues
) {
    val taskListState by taskManager.tasks.observeAsState(initial = emptyList())
    var data by remember { mutableStateOf(taskListState) }

    LaunchedEffect(taskListState) {
        data = taskListState
    }

    val state = rememberReorderableLazyListState(dragCancelledAnimation = NoDragCancelledAnimation(),
        onMove = { from, to ->
            data = data.toMutableList().apply {
                add(to.index, removeAt(from.index))
            }
            taskManager.updateTasks(data)
        })


    LazyColumn(
        contentPadding = innerPadding,
        state = state.listState,
        modifier = Modifier
            .reorderable(state)
            .detectReorderAfterLongPress(state)
    ) {
        items(data, { it.id }) { item ->
            ReorderableItem(state, key = item.id, defaultDraggingModifier = Modifier) { isDragging ->
                val elevation = animateDpAsState(if (isDragging) 16.dp else 0.dp, label = "")
                Column(
                    modifier = Modifier
                        .shadow(elevation.value)
                        .draggedItemModifier(isDragging)
                ) {
                    CreateTasks(item, taskManager, navController)
                }
            }
        }
    }
}
fun Modifier.draggedItemModifier(isDragging: Boolean): Modifier {
    return this.then(if (isDragging) Modifier else Modifier)
}
@RequiresApi(Build.VERSION_CODES.O)
fun isTaskLate(taskDateInMillis: Long): Boolean {
    val currentDate = LocalDate.now(ZoneId.systemDefault())
    val taskDate = Instant.ofEpochMilli(taskDateInMillis)
        .atZone(ZoneId.systemDefault())
        .toLocalDate()

    return taskDate.isBefore(currentDate)
}
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CreateTasks(
    task: Task,
    taskManager: TaskListManager,
    navController: NavController,
){
    var menuExpanded by remember {
        mutableStateOf(false)
    }

    val dateFormat = SimpleDateFormat("dd/MM/yyyy")
    Column{
        task.deadLine?.let {
            Row {
                Text(
                    color = Color(0xFF8e5ce6 ),
                    maxLines = 1,
                    text = " ${task.deadLine.let { "A faire pour le: ${dateFormat.format(it)}" }}",
                    modifier = Modifier.padding(10.dp, 0.dp),
                    softWrap = true,
                    fontSize = 15.sp,
                    fontStyle = FontStyle.Italic,
                )
                if (isTaskLate(task.deadLine) and task.toDo) Text(
                    maxLines = 1,
                    text =  " En retard",
                    softWrap = true,
                    fontSize = 15.sp,
                    fontStyle = FontStyle.Italic,
                    color = Color(0xFF341172)
                )
            }
        }
        Card(
            colors = CardDefaults.cardColors(
                containerColor = Color(0xFF8e5ce6  ),
                contentColor = Color.White,
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp, task.deadLine?.let { 0.dp } ?: 10.dp, 10.dp, 10.dp)
        ) {

            Row (
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()
            ){
                Log.e("ressource",task.logo.toString() )
                Image(
                    painter = painterResource(id = task.logo),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(10.dp)
                )
                Text(
                    text = task.text,
                    modifier = Modifier
                        .padding(10.dp)
                        .width(170.dp),
                    softWrap = true
                )
                Row(
                    verticalAlignment = Alignment.CenterVertically){
                    Button(
                        contentPadding = PaddingValues(0.dp),
                        modifier= Modifier
                            .width(60.dp)
                            .height(20.dp),
                        onClick = { taskManager.updateIsDoneStatusOfTask(task) },
                        colors = if (task.toDo) ButtonColors(Color.White,Color(0xFF8e5ce6),Color.White, Color.White  ) else ButtonColors(Color(0xFF341172),Color.White,Color.White, Color.White  )
                    ) {
                        Text(text = if (task.toDo) "A FAIRE" else "FAIT")
                    }
                    IconButton(onClick = { menuExpanded = !menuExpanded }) {
                        Icon(
                            imageVector = Icons.Filled.MoreVert,
                            contentDescription = "More",
                        )
                    }

                    DropdownMenu(
                        expanded = menuExpanded,
                        onDismissRequest = { menuExpanded = false },
                    ) {
                        DropdownMenuItem(
                            text = {
                                Text("Supprimer")
                            },
                            leadingIcon = {Icon(Icons.Rounded.Delete, contentDescription = null)},
                            onClick = {taskManager.remove(task)},
                        )
                        DropdownMenuItem(
                            text = {
                                Text("Modifier")
                            },
                            leadingIcon = {Icon(Icons.Rounded.Edit, contentDescription = null)},
                            onClick = {navController.navigate("AddTask/" + task.id)},
                        )
                    }
                }
            }
        }
    }
}
